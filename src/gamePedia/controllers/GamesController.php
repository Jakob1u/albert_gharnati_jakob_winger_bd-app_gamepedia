<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 15/03/2016
 * Time: 17:25
 */

namespace gamePedia\controllers;
use gamePedia\models\Game;
use gamePedia\models\Commentaire;
use gamePedia\models\Character;
use Slim\Slim;

class GamesController
{

    public function getGameId($id) {
        $g = Game::where('id', '=', $id)->select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')->get();
        $app = Slim::getInstance();
        $res = array('game' => $g, 'links' => array('comments' => array('href' => $app->urlFor('gameComments', array('id' => $id))), 'characters' => array('href' => $app->urlFor('gameCharacters', array('id' => $id)))));
        $app->response->headers->set('Content-Type', 'application/json');
        echo json_encode($res);
    }

    public function getGameCollection($nb) {
        if ($nb == null) {
            $g = Game::select('id', 'name', 'alias', 'deck')->get();
        }
        else {
            $g = Game::select('id', 'name', 'alias', 'deck')->skip(200 * ($nb - 1))->take(200)->get();
        }
        $app = Slim::getInstance();
        $res2 = array();
        $app->response->headers->set('Content-Type', 'application/json');
        foreach($g as $k => $v) {
            $res2[] = array('game' => $v, 'links' => array('self' => array('href' => $app->urlFor('gameId', array('id' => $v->id)))));
        }

        //cas particulier de la premi�re page
        if($nb == 1)
        $res = array('games' => $res2, 'links' => array('next' => array('href' => $app->urlFor('gamesCollection') . "?" . http_build_query(array( "page"=>$nb+1)))));
        else
        $res = array('games' => $res2, 'links' => array('prev' => array('href' => $app->urlFor('gamesCollection') . "?" . http_build_query(array( "page"=>$nb-1))), 'next' => array('href' => $app->urlFor('gamesCollection') . "?" . http_build_query(array( "page"=>$nb+1)))));
        echo json_encode($res) ;
    }

    public function getGameComments($id) {
        $g = Commentaire::where('game_id', '=', $id)->select('id', 'titre', 'message', 'date', 'user_id')->get();
        $app = Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        echo json_encode($g);
    }

    public function getGameCharacters($id) {
        $g = Game::find($id)->characters()->select('id', 'name', 'alias', 'deck', 'description', 'created_at')->get();
        $app = Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        $res = array('characters' => $g);
        echo json_encode($res);
    }
}