<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class RatingBoard extends Model{

    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function ratings(){
        return $this->hasMany('\gamePedia\models\GameRating','id');
    }
}