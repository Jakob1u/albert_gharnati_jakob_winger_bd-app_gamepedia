<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Commentaire extends Model{

    protected $table = 'commentaire';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function game(){
        return $this->belongsTo('\gamePedia\models\Game','game_id');
    }

    public function user() {
        return $this->belongsTo('\gamePedia\models\Utilisateur', 'user_id');
    }

}