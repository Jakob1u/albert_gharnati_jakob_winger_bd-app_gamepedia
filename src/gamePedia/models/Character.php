<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Character extends Model{

    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->belongsToMany('\gamePedia\models\Game', 'game2character', 'character_id', 'game_id');
    }

    public function friends() {
        return $this->belongsToMany('\gamePedia\models\Character', 'friends', 'char1_id', 'char2_id');
    }

    public function enemies() {
        return $this->belongsToMany('\gamePedia\models\Character', 'enemies', 'char1_id', 'char2_id');
    }

}