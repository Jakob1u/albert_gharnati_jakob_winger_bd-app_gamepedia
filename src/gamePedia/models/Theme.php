<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Theme extends Model{

    protected $table = 'theme';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function games() {
        return $this->belongsToMany('\gamePedia\models\Game', 'game2theme', 'theme_id', 'game_id');
    }
}