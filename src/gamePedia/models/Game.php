<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Game extends Model{

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function firstAppear() {
        return $this->hasMany('\gamePedia\models\Character', 'first_appeared_in_game_id');
    }
	
	public function characters() {
		return $this->belongsToMany('\gamePedia\models\Character', 'game2character', 'game_id', 'character_id');
	}

    public function comments() {
        return $this->hasMany('\gamePedia\models\Commentaire', 'game_id');
    }

    public function original_game_ratings() {
        return $this->belongsToMany('\gamePedia\models\GameRating', 'game2rating', 'game_id', 'rating_id');
    }

    public function publishers() {
        return $this->belongsToMany('\gamePedia\models\Company', 'game_publishers', 'game_id', 'comp_id');
    }

    public function developers() {
        return $this->belongsToMany('\gamePedia\models\Company', 'game_developers', 'game_id', 'comp_id');
    }

    public function similar_games() {
        return $this->belongsToMany('\gamePedia\models\Game', 'similar_games', 'game1_id', 'game2_id');
    }

    public function genres() {
        return $this->belongsToMany('\gamePedia\models\Genre', 'game2genre', 'game_id', 'genre_id');
    }

    public function themes() {
        return $this->belongsToMany('\gamePedia\models\Theme', 'game2theme', 'game_id', 'theme_id');
    }

}