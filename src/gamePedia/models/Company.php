<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Company extends Model{

    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function platforms(){
        return $this->hasMany('\gamePedia\models\Platform','id');
    }

    public function hasPublished() {
        return $this->belongsToMany('\gamePedia\models\Company', 'game_publishers', 'comp_id', 'game_id');
    }

    public function games() {
        return $this->belongsToMany('\gamePedia\models\Company', 'game_developers', 'comp_id', 'game_id');
    }

}