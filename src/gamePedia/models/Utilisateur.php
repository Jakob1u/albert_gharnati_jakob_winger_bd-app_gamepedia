<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model{

    protected $table = 'utilisateurs';
    protected $primaryKey = 'email';
    public $timestamps = false;

    public function commentaires(){
        return $this->hasMany('\gamePedia\models\Commentaire','user_id');
    }

}