<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class Platform extends Model{

    protected $table = 'platform';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function producer(){
        return $this->belongsTo('\gamePedia\models\Company','c_id');
    }

}