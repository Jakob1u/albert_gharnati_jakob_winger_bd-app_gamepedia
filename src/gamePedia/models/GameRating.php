<?php

namespace gamePedia\models;
USE \Illuminate\Database\Eloquent\Model;

class GameRating extends Model{

    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function ratingBoard(){
        return $this->belongsTo('\gamePedia\models\RatingBoard','id');
    }

    public function original_game() {
        return $this->belongsToMany('\gamePedia\models\Game', 'game2rating', 'rating_id', 'game_id');
    }
}