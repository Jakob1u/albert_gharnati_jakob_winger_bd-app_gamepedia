<?php
/**
 * Created by PhpStorm.
 * User: jakob1u
 * Date: 15/03/2016
 * Time: 15:34
 */

require '../../../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../../../src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$faker = Faker\Factory::create();
for ($i = 0; $i < 100; $i++) {
    $utilisateur = new \gamePedia\models\Utilisateur();
    $utilisateur->email = $faker->email;
    $utilisateur->nom = $faker->lastName;
    $utilisateur->prenom = $faker->firstName;
    $utilisateur->password = password_hash($faker->password, PASSWORD_DEFAULT);
    $utilisateur->save();
}

//for ($i = 0; $i < 1500; $i++) {
//    $commentaire = new \gamePedia\models\Commentaire();
//    $commentaire->message = $faker->text;
//    $commentaire->titre = $faker->text;
//    $commentaire->date = $faker->dateTime($max = 'now');
//    $commentaire->game_id = $faker->numberBetween(1,10000);
//    $commentaire->user_id = $faker->numberBetween(1, 300);
//    $commentaire->save();
//}