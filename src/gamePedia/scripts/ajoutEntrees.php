<?php
require '../../../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use gamePedia\models\Game;
use gamePedia\models\Company;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../../../src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

//$u1 = new \gamePedia\models\Utilisateur();
//$u1->email = "aaaa@aaaa.com";
//$u1->nom = "Sadly";
//$u1->prenom = "Charles";
//$u1->save();
//
//$u2 = new \gamePedia\models\Utilisateur();
//$u2->email = "bbbb@bbbb.com";
//$u2->nom = "Happy";
//$u2->prenom = "Luc";
//$u2->save();

$user1 = \gamePedia\models\Utilisateur::find(1);
$user2 = \gamePedia\models\Utilisateur::find(2);

//$c1 = new \gamePedia\models\Commentaire();
//$c1->message = "Super jeu !";
//$c1->game_id = 12342;
//$c1->user_id = $user1->email;
//$c1->save();
//
//$c2 = new \gamePedia\models\Commentaire();
//$c2->message = "Nul :(";
//$c2->game_id = 12342;
//$c2->user_id = $user1->email;
//$c2->save();
//
//$c3 = new \gamePedia\models\Commentaire();
//$c3->message = "Moyen";
//$c3->game_id = 12342;
//$c3->user_id = $user1->email;
//$c3->save();
