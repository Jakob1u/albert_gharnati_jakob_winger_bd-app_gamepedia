<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use gamePedia\models\Game;
use gamePedia\models\Company;
use Slim\Slim;
use gamePedia\controllers\GamesController;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

DB::enableQueryLog();

//SEANCE 2

/*
//echo all game
$gameall = Game::all();
foreach ($gameall as $key) {
	echo $key->name.' : '.$key->deck."\n";
}
*/

/*
//echo 442 game a partir du 21173em
$game = Game::take(442)->skip(21172)->get();
foreach ($game as $key) {
	echo $key->name.' : '.$key->deck."\n";
}
*/

//$mario = Game::where("name","LIKE","%Mario%")->get();
//foreach ($mario as $key) {
//	//echo $key->name.' : '.$key->deck."<br>";
//}

/*
$compa = Compagny::where("location_country","=","Japan")->get();
foreach ($compa as $key) {
	echo $key->name.' : '.$key->deck."<br>";
}
*/

/**
 * Question 5
 */
/*
$plateformes = Platform::where('install_base', '>=' ,10000000)->get();
foreach ($plateformes as $pf) {
	echo $pf->name, "<br>";
}
*/

//SEANCE 3

$time_start = microtime(true);

/**
* Question 1
*/

//$games = Game::find(12342);
//$char = $games->characters;
//foreach ($char as $key) {
//	echo $key->name." / ".$key->deck."<br>";
//}

/**
 * Question 2
 */

//$games = Game::where('name', 'LIKE', 'Mario%')->get();
//foreach($games as $g) {
////	//echo $g->name . ' : ';
//	$char = $g->characters;
////	foreach ($char as $c) {
////		//echo $c->name . "<br>";
////	}
//}


/**
* Question 3
*/

// $companies = Company::where('name', 'LIKE', '%Sony%')->get();
//foreach ($companies as $c) {
//	//echo $c->name;
//	$games = $c->games;
////	foreach ($games as $g) {
////		echo $g->name . "<br>";
////	}
//}



/**
* Question 4
*/ 

/* $games = Game::where('name', 'LIKE', '%Mario%')->get();
foreach ($games as $key) {
	$lg = $key->original_game_ratings;
	echo $key->name." : ";
	foreach($lg as $value) {
		echo $value->name." : ".$value->ratingBoard->name." / ";
	}
	echo "<br>";	
} */


/**
 * Question 5
 */

/* $games = Game::where('name', 'LIKE', 'Mario%')->get();
foreach ($games as $g) {
	if ($g->characters()->count() > 3) {
		echo $g->name . "<br>";
	}
} */

/* $games = Game::where('name', 'LIKE', 'Mario%')->has('characters', '>', 3)->get();
foreach ($games as $g) {
	echo $g->name . "<br>";
} */

/**
 * Question 6
 */

/* $games = Game::where('name', 'LIKE', 'Mario%')
			  ->whereHas('original_game_ratings', function($q) {
				 $q->where('name', 'LIKE', '%3+%');
			  })->get();
foreach ($games as $g) {
	echo $g->name . "<br>";
} */


/**
 * Question 7
 */
// $games = Game::where('name', 'LIKE', 'Mario%')
//	->whereHas('publishers', function($q) {
//		$q->where('name', 'like','%Inc.%');
//	})
//	 ->has('original_game_ratings', 'like', '%3+%')
//	->get();
//foreach($games as $key => $val) {
//	echo $val->name;
//}

/**
 * Question 8
 */

//$games = Game::where('name', 'LIKE', 'Mario%')
//			 ->whereHas('publishers', function($q) {
//				 $q->where('name', 'LIKE', '%Inc%');
//			   })
//			 ->whereHas('original_game_ratings', function($q) {
//				 $q->where('name', 'LIKE', '%+3%');
//				 $q->whereHas('rating', function($r) {
//					 $r->where('name', '=', 'CERO');
//				 });
//			   })->get();
//foreach ($games as $g) {
//	echo $g->name;
//}


// CHARGEMENT LI�
//$characters = \gamePedia\models\Character::with(['games' => function ($query) {
//	$query->where('name', 'like', '%Mario%');
//}])->get();

//$games = Game::with('characters')->where('name', 'like', '%Mario%')->get();

//$company = Company::with('games')->where('name', 'like', '%Sony%')->get();
//
//$time_end = microtime(true);
//$time = $time_end - $time_start;
//echo "Temps pour executer la requete " . $time . " msec sec" . "<br>";
//
//
//$queries = DB::getQueryLog();
//$n = 1;
//$n2 = 1;
//$tempsTotal = 0;
//foreach($queries as $q) {
//    echo "Query $n : " . $q['query'] . PHP_EOL;
//    $n++;
//    foreach($q['bindings'] as $b) {
//        echo "     Parametre $n2 : " . $b . PHP_EOL;
//        $n2++;
//    }
//    echo PHP_EOL;
//    $tempsTotal += $q['time'];
//}
//echo "Temps total : " . $tempsTotal . PHP_EOL;


$app = new Slim();

$app->get('/', function() {
	echo "lol";
})->name('home');

$app->get('/api/games/:id', function($id) {
	$c = new GamesController();
	$c->getGameId($id);
})->name('gameId');

$app->get('/api/games', function() use($app) {
	$c = new GamesController();
	$c->getGameCollection($app->request()->get('page'));
})->name('gamesCollection');

$app->get('/api/games/:id/comments', function($id) {
	$c = new GamesController();
	$c->getGameComments($id);
})->name('gameComments');

$app->get('/api/games/:id/characters', function($id) {
	$c = new GamesController();
	$c->getGameCharacters($id);
})->name('gameCharacters');

$app->run();

			
	


